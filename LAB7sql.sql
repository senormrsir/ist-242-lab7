CREATE DATABASE Lab7;
USE Lab7;
CREATE TABLE peopleData (
    LastName VARCHAR(40),
    FirstName VARCHAR(40),
    hourlyWorked FLOAT(10),
    PayRate FLOAT(10)
);
INSERT INTO peopledata(LastName, FirstName, hourlyWorked, payRate) Values('Smith', 'Seth', 55.5, 12.50);
INSERT INTO peopledata(LastName, FirstName, hourlyWorked, payRate) Values('Jones', 'Adam', 40.5, 5.50);
INSERT INTO peopledata(LastName, FirstName, hourlyWorked, payRate) Values('Davis', 'Chris', 50.0, 10.00);
INSERT INTO peopledata(LastName, FirstName, hourlyWorked, payRate) Values('Machado', 'Manny', 45.5, 5.50);
INSERT INTO peopledata(LastName, FirstName, hourlyWorked, payRate) Values('Hardy', 'J.J.', 40.0, 17.25);
INSERT INTO peopledata(LastName, FirstName, hourlyWorked, payRate) Values('Schoop', 'Jon', 45.0, 23.80);
INSERT INTO peopledata(LastName, FirstName, hourlyWorked, payRate) Values('Rickard', 'Joey', 40.0, 35.00);
INSERT INTO peopledata(LastName, FirstName, hourlyWorked, payRate) Values('Walker', 'Chris', 25.5, 13.00);
INSERT INTO peopledata(LastName, FirstName, hourlyWorked, payRate) Values('Trumbo', 'Mark', 30.0, 11.00);
INSERT INTO peopledata(LastName, FirstName, hourlyWorked, payRate) Values('Joseph', 'Caleb', 20.5, 10.00);
