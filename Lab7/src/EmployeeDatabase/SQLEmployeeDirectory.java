/**********************************************************
*  Filename:          		SQLEmployeeDirectory.java
*  Package:                 EmployeeDatabase
*  Project:                 Lab 7:db
*  Author(s):               Stephen Hengeli / Konstantinos Koutris
*  Section:                 IST 242.001 / MIS 307.001
*  Assignment:        		Lab 7:db
*  Description:       		Read info from MySQL database and print
*  Date Created:      		4/10/2018
*  Date Modified:			4/12/2018
*  Modifier:				Stephen Hengeli
*  Changes:					Syntax of SQL statement being used with the executeQuery method [line 28]
*********************************************************/
package EmployeeDatabase;

import java.sql.*;		// imports
import java.util.*;

public class SQLEmployeeDirectory {

	public List<Employee> getEmployee() {		// this method gets the list of employees
		ArrayList<Employee> employees = new ArrayList<Employee>();		// ArrayList to make the array to hold the employee list
		try {
				// try to get the connection using the JDBC driver
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("Driver Loaded");
			Connection conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/Lab7", "root", "bradleyhengeli1");	//Opens the database using the username 'root' and the password was the root password for MySQL on my laptop.
			PreparedStatement st = conn.prepareStatement("SELECT * FROM peopledata");	// prepares the statement to show the table. Language: SQL......All caps is usual syntax.
			ResultSet rs = st.executeQuery();		// use 'ResultSet' to create a table from the SQL database table. execute the SQL statement from the prepared statement method
			System.out.println("Query Executed");
			//loop through the data from MySQL and set each of the variables using an instance of the class 'Employee'
			while (rs.next()) {
				Employee emp = new Employee();
				emp.setFirstName(rs.getString("firstName"));
				emp.setLastName(rs.getString("lastName"));
				emp.setHourlyWorked(rs.getDouble("hourlyWorked"));
				emp.setPayRate(rs.getDouble("payRate"));
				employees.add(emp);						// put the info from MySQL into a arraylist
			}//while
		} catch (Exception e) {		// if the try statement above isn't satisfied, throw this exception and print message/problem
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}//try/catch
		return employees;					// return the list of employees
	}//getEmployee
public static void main(String[] args) {
	SQLEmployeeDirectory obj = new SQLEmployeeDirectory();			// create a object of CalculateGross class
	String strfirstName;		// variables
	String strlastName;
	double dhoursWorked;
	double dpayrate;
	double doverTime = 0.00;		// initializing these to '0'
	double dgrossPay = 0.00;
	double dregPay;
	double dotPay;
	double dtotalPay = 0.00;
	ArrayList<Employee> employeesList = (ArrayList<Employee>) obj.getEmployee();		// calls method to get array of employees
	System.out.println("Name \t\t Hours \t OTHour \t PayRates \t RegularPay \t OT Pay \t Gross pay");			// prints a heading
	System.out.println("-----------------------------------------------------------------------------------------------------");		// prints a dashed line
	for (Employee list : employeesList) {				// gets the list of each employee from MySQL and processes on it's own
		strfirstName = list.getFirstName();
		strlastName = list.getLastName();
		dhoursWorked = list.getHourlyWorked();
		dpayrate = list.getPayRate();
		if (dhoursWorked >= 40) {				// evaluates hours
			doverTime = dhoursWorked - 40;
			dgrossPay = ((dhoursWorked-doverTime) * dpayrate) + (doverTime * dpayrate * 1.5);			// calculates gross pay
			dregPay = (dhoursWorked-doverTime) * dpayrate;			// calculates regular pay
			dotPay = doverTime * dpayrate * 1.5;		// calculates OT pay
		}//if
		else {
			dregPay = dhoursWorked * dpayrate;		// calculates regular pay
			dotPay = doverTime * dpayrate * 1.5;		// calculates OT pay
			dgrossPay = (dhoursWorked * dpayrate) + (doverTime * dpayrate * 1.5);		// calculates gross pay
		}//else
		// [BELOW] print table
		System.out.println(strlastName + "," + strfirstName + " \t " +String.format( "%.0f", dhoursWorked ) + " \t " + String.format( "%.0f", doverTime ) + " \t \t "+"$ "+String.format( "%.2f", dpayrate ) + " \t " + "$ "+ String.format( "%.2f", dregPay )+ " \t " + "$ "+String.format( "%.2f", dotPay ) +" \t " +"$ "+ String.format( "%.2f", dgrossPay ));
		dtotalPay = dtotalPay + dgrossPay;		// calculate total pay
	}//for
	System.out.println("-----------------------------------------------------------------------------------------------------");
	System.out.println("Department Total Pay: \t\t\t\t\t\t\t\t\t$ " + String.format( "%.2f", dtotalPay ));		// prints total pay
	}//main
}//class
