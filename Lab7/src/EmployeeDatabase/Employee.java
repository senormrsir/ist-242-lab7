/**********************************************************
*  Filename:          		Employee.java
*  Package:                 EmployeeDatabase
*  Project:                 Lab 7:db
*  Author(s):               Stephen Hengeli / Konstantinos Koutris
*  Section:                 IST 242.001 / MIS 307.001
*  Assignment:        		Lab 7:db 
*  Description:       		Sets and Gets variables
*  Date Created:      		4/10/2018
*  Date Modified:			n/a
*  Modifier:				n/a
*  Changes:					n/a
*********************************************************/
package EmployeeDatabase;

public class Employee {

	private String strfirstName;		// variables
	private String strlastName;
	private Double dhourlyWorked;
	private Double dpayRate;

	public Employee() {	

	}// constructor
	public String getLastName() {return strlastName;}// gets last name

	public void setLastName(String lastName) {this.strlastName = lastName;}// sets last name

	public String getFirstName() {return strfirstName;}//gets first name
	
	public void setFirstName(String firstName) {this.strfirstName = firstName;}//sets first name
	
	public Double getHourlyWorked() {return dhourlyWorked;}//gets first name
	
	public void setHourlyWorked(Double hourlyWorked) {this.dhourlyWorked = hourlyWorked;}//sets first name
	
	public Double getPayRate() {return dpayRate;}//gets pay rate

	public void setPayRate(Double payRate) {this.dpayRate = payRate;}//sets pay rate

}// class
